module Options
  def Options.load_command_line
    # Read the options from the command line
    opts = Trollop::options do
      version "Mailchimp Import Script 1.0"
      banner <<-EOS
Mailchimp Import Script imports users from the DB into Mailchimp.

It is assumed that users have already signed up to receive the newsletter, hence no confirmation emails will be sent to the users.

Usage:
  subscribe_users_to_mailchimp [options] <filenames>+
where [options] are:
EOS
      opt :environment, "Running environment", :default => 'development', :type => String, :short => 'e'
      opt :dry_run, "Do not do anything", :default => false
    end
    # Trollop::die :environment, "must be one of 'development' or 'production'" if !(%w('environment' 'production').include?(opts[:environment]))
    return opts
  end

  def Options.load_config(environment)
    # Script directory
    path = File.expand_path(File.dirname(__FILE__))
    config = YAML.load_file(path + "/mailchimp_config.yml")[environment]
    return config
  end
end