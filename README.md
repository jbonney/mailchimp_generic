# Import users from a DB to Mailchimp

The script connects to a DB to extract users and send contact details to Mailchimp. Execution times of the script are registered into a history CSV file that is used between each request to only query the DB about the entries added since the last execution time.

If errors occur during submission to Mailchimp, they are registered in a CSV file that is sent to an administrator. The administrator can then take the necessary action and enter them manually into Mailchimp (for instance).

Deploying is pretty simple and only requires a few steps:

1. Add your configuration details in `mailchimp_config.yml`
2. Install the needed Gems
3. Set up the history.csv file
4. Dry run the script
5. Set up a cron job to run on a regular basis

## Configure the script to include your personal information

All settings are available in the `mailchimp_config.yml``file. Configure your SMTP server, email information, DB connection parameters and Mailchimp API key and list information.

In addition, depending on the structure of your DB, you might need to override the model defined in the script.

## Install the needed Gems

A few dependencies are required. Gems are bundled in a Gemfile so simply run:

    bundle install

## Set up the history.csv file

Execution times are registered in a CSV file. This allows to query the DB for data that would have been imported since the last execution. Set up the first date to look from in the history.csv file. Simply use the format already in place.

    Last Execution Time,Accounts to import,Accounts imported
    2012-05-12 16:29:29 +0200,0,0

## Dry run the script

Verify that everything is running as it should.

    chmod +x subscribe_users_to_mailchimp.rb
    ./subscribe_users_to_mailchimp.rb

## Set up a cron job to run on a regular basis

Define a frequency at which the script should run.

    0 * * * * /path/to/zoho/script/subscribe_users_to_mailchimp.rb -e production > /path/to/export/export.log 2>/dev/null

### License

This script is released under the [MIT license](http://www.opensource.org/licenses/MIT "MIT license").

### Acknowledgement

This script was initially put together for [D-Sight](http://www.d-sight.com/ "D-Sight Decision Making") for their web application [D-Sight Web](https://web.d-sight.com "D-Sight Web"). Special thanks for allowing publishing it.