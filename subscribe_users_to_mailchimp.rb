#!/usr/bin/env ruby

# Import users from a MySQL DB to Mailchimp
# @author: Jimmy Bonney (jbonney@d-sight.com)
# @created_at: 2012-11-01
# @sponsor: D-Sight (http://www.d-sight.com)
# @version: 1.0

# Dependencies
require 'rubygems'
require 'active_record'
require 'logger'
require 'httparty'
require 'csv'
require 'mail'
require 'gibbon'
require 'yaml'
require 'trollop'
require_relative 'options'

# Get options from command line
options = Options.load_command_line
# Identify if running in dry-run mode
@@dry_run = options[:dry_run]

# Read config file based on the environment received in option
config = Options.load_config(options[:environment])

# Load email setup
Mail.defaults do
  delivery_method :smtp, {
    :address              => config[:email][:address],
    :port                 => config[:email][:port],
    :domain               => config[:email][:domain],
    :user_name            => config[:email][:user_name],
    :password             => config[:email][:password],
    :authentication       => config[:email][:authentification],
    :enable_starttls_auto => config[:email][:enable_starttls_auto] }
end

# Set up active record connection
ActiveRecord::Base.establish_connection(
  :adapter  => config[:db][:adapter],
  :encoding => config[:db][:encoding],
  :database => config[:db][:database],
  :username => config[:db][:username],
  :password => config[:db][:password],
  :host     => config[:db][:host],
  :timeout  => config[:db][:timeout]
)

class MultiDelegator
  def initialize(*targets)
    @targets = targets
  end

  def self.delegate(*methods)
    methods.each do |m|
      define_method(m) do |*args|
        @targets.map { |t| t.send(m, *args) }
      end
    end
    self
  end

  class <<self
    alias to new
  end
end

# Define interactions with mailchimp
class Monkey
  # Define public attributes as seen in
  # http://stackoverflow.com/questions/982848/dryer-object-initialization-in-ruby
  ATTRIBUTES = [:api_key, :list_id, :error_hash].freeze

  # Define attr_accessor
  ATTRIBUTES.each do |attribute|
    attr_accessor attribute
  end

  # Set default values based on the options
  DEFAULTS = {
    :error_hash => Hash.new
  }.freeze

  # Set private variable
  @gibbon = nil

  def initialize(args)
    # merge default args with args provided to the constructor
    args = args ? DEFAULTS.merge(args) : DEFAULTS
    # Initialize attributes
    ATTRIBUTES.each do |attribute|
      if (args.key?(attribute))
        instance_variable_set("@#{attribute}", args[attribute])
      end
    end
    # Raise exception if no API key is provided
    raise ArgumentError, 'API key is required to contact Mailchimp' unless @api_key

    # Set Gibbon (mailchimp API)
    @gibbon = Gibbon.new(@api_key)
    @gibbon.throws_exceptions = false
  end


  #
  # Subscribe users in batch to the list
  # @param  users [Array<Hash>] Array of hashes containing user information in the form {:email_address => ..., :first_name => ..., :last_name => ...}
  #
  # @return [type] [description]
  def batch_subscribe(users = [])
    raise ArgumentError, 'User list required' if users.empty?

    # Send the request
    unless @@dry_run
      subscriptions = @gibbon.list_batch_subscribe(:id => @list_id, :double_optin => false, :batch => convert_user_hashes(users))

      # Fill in the error hash in case of error returned by Mailchimp
      if subscriptions["error_count"] > 0
        subscriptions["errors"].each do |error|
          @error_hash[error["email"]] = error["message"]
        end
      end
    end
  end

  #
  # Subscribe a single user to the list
  # @param [Hash] user_details User options to be sent to Mailchim
  # @option user_details [string] :email_address The email address to subscribe
  # @option user_details [string] :first_name The first name of the user
  # @option user_details [string] :last_name The last name of the user
  #
  # @return [Boolean] true if request to Mailchimp succeeds
  # @return [Hash] a hash with email_address as key and error message as value otherwise if mailchimps returns an error
  def subscribe(user_details = {})
    # Ensure that the email address is given
    email_address = user_details.key?(:email_address) ? user_details[:email_address] : nil
    raise ArgumentError, 'Email address required' unless email_address

    # Convert arguments to be sent
    user_information = convert_user_details(user_details)
    unless @@dry_run
      # Send the request
      subscription = @gibbon.list_subscribe({:id => @list_id, :double_optin => false, :email_address => email_address}.merge(user_information))
      # Fill in the error hash in case of error returned by Mailchimp
      unless subscription == true
        @error_hash[email_address] = subscription["error"]
      end
    end
  end

  private
  #
  # Prepare the merge_vars hash to be used by list_subscribe function
  # @param  user_details [Hash] The hash containing the first name and last name
  #
  # @return [Hash] A hash of the form {:merge_vars => {:FNAME => xyz, :LNAME => tuv}}
  def convert_user_details(user_details)
    merge_vars = {}
    if user_details.key?(:first_name) || user_details.key?(:last_name)
      first_name = {:FNAME => user_details.key?(:first_name) ? user_details[:first_name] : ""}
      last_name = {:LNAME => user_details.key?(:last_name) ? user_details[:last_name] : ""}
      merge_vars = {:merge_vars => first_name.merge(last_name)}
    end
    return merge_vars
  end

  #
  # Convert a user array of hashes in the form {:email_address => ..., :first_name => ..., :last_name => ...} to {:email_address => ..., :FNAME => ..., :LNAME => ...}
  # @param  users [Array<Hash>] Array of hashes containing user information in the form {:email_address => ..., :first_name => ..., :last_name => ...}
  #
  # @return [Array<Hash>] Array of hashes containing user information in the form {:email_address => ..., :FNAME => ..., :LNAME => ...}
  def convert_user_hashes(users = [])
    map = {:email_address => :EMAIL, :first_name => :FNAME, :last_name => :LNAME}
    new_users = []
    # Go through the user list
    users.each do |user|
      user_details = {}
      user.each do |key, value|
        user_details[map[key]] = value
      end
      new_users << user_details
    end
    return new_users
  end
end

# [UPDATE WITH YOUR MODELS AS DEFINED IN YOUR APPLICATION]
# Users
# The user model contains email, first name, last name, etc...
class User < ActiveRecord::Base
end

# Rotate the log file every 10MB but do not load the logger here
Logger.new 'mailchimp.log', 0, 10 * 1024 * 1024
# Open the logg file in add mode
log_file = File.open("mailchimp.log", "a")
# Set up the logger. In dry run, right only to STDOUT, otherwise both in STDOUT and the log file.
if @@dry_run
  log = Logger.new MultiDelegator.delegate(:write, :close).to(STDOUT)
else
  log = Logger.new MultiDelegator.delegate(:write, :close).to(STDOUT, log_file)
end

# redirect active record output to the log
ActiveRecord::Base.logger = log

log.info config

# Script directory
path = File.expand_path(File.dirname(__FILE__))
# Get the historical data
previous_executions = CSV.read(path + "/history.csv", :headers => :true)
# Get the last execution data
last_execution = previous_executions[previous_executions.size-1]
# Get the last execution date (this is the first column)
last_date = DateTime.parse last_execution['Last Execution Time']
log.info "Last execution date: #{last_date}"

# Set the new execution date (now)
new_date = DateTime.parse(DateTime.now.to_s).utc
log.info "Current execution date: #{new_date}"
# Set the date format used in file name
date_info = new_date.strftime("%Y-%m-%dT%H:%M:%S")

# Get the users created during the period [last_date, new_date]
users = User.where("created_at > ? and created_at < ?", last_date, new_date - 1.seconds).all
log.info "Number of users created since last time: #{users.count}"

# Initialize Monkey object
monkey = Monkey.new(config[:mailchimp])

# Extract the users (batch of 5000 users max so that we can subscribe by batch)
users.each_slice(5000) do |batch|
  subscription_details = []
  # Gather only useful attributes
  batch.map do |user|
    subscription_details << {:email_address => user.email, :first_name => user.first_name, :last_name => user.last_name}
  end
  # subscribe users to mailchimp
  monkey.batch_subscribe subscription_details
end

# Process errors (if any) that occurred during the subscription
unless monkey.error_hash.empty?
  # Create CSV header
  CSV.open(path + "/" + date_info + "_manual_import.csv", "a") do |csv|
    csv << ['Date', 'User ID', 'Email', 'First Name', 'Last Name', 'Error Details']
  end
  CSV.open(path + "/" + date_info + "_manual_import.csv", "a") do |csv|
    monkey.error_hash.each do |email, message|
      user = User.where(:email => email).first
      csv << [new_date, user.id, email, user.first_name, user.last_name, message]
    end
  end
end

# Save history (only when not in dry-run)
successfully_saved = users.count - monkey.error_hash.count
unless @@dry_run
  CSV.open(path + "/history.csv", "a") do |csv|
    csv << [new_date, users.count, successfully_saved]
  end
  log.info "History updated"
else
  log.info "History file is not updated during dry-run"
end

# [UPDATE WITH YOUR VALUES]
# Send email in case of failure
if successfully_saved != users.count
  if @@dry_run
    log.info "In dry-run mode, no emails are sent out"
  else
    log.info "Sending email to inform administrator that manual import to Mailchimp is required"
    mail = Mail.deliver do
      to config[:email][:to]
      from config[:email][:from]
      subject config[:email][:subject]
      text_part do
        body "An error occured while importing contacts to Mailchimp. Please take a look at the attached CSV file and import manually in Mailchimp."
      end
      html_part do
        content_type 'text/html; charset=UTF-8'
        body "An error occured while importing contacts to Mailchimp. Please take a look at the attached CSV file and import manually in Mailchimp."
      end
      attachments[date_info + "_manual_import.csv"] = {
        :content => File.read(path + "/" + date_info + "_manual_import.csv")
      }
    end
  end
end

unless @@dry_run
  log.info "Imported #{successfully_saved}/#{users.count} users to Mailchimp successfully" if users.count > 0
  log.info "Error log is available at #{path}/#{date_info}_manual_import.csv" if successfully_saved != users.count
else
  log.info "Would try to import #{users.count} users to Mailchimp"
end
